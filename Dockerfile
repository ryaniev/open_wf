FROM python
RUN apt update -y && apt install -y python-pip && groupadd -g 1000 encoder \
  && useradd -m -g encoder -u 1000 encoder \
  && mkdir -p /home/encoder/watchbucket/
WORKDIR /home/encoder/watchbucket/
COPY . /home/encoder/watchbucket/
RUN chmod -R 770 /home/encoder/
#USER encoder
RUN python2 -m pip install -r ./watchbucket/requirements.txt
CMD ["python2","./watchbucket/watchbucket.py"]